package com.cursos.alternativos.utils;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class Commons {
	
	public static DataSource getDs() throws NamingException{
		InitialContext ctx = new InitialContext();
		DataSource ds =(DataSource) ctx.lookup("java:/comp/env/jdbc/bd_escuela");
		return ds;
	}

}
