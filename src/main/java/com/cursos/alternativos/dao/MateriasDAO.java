package com.cursos.alternativos.dao;

import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.cursos.alternativos.dto.MateriasDTO;
import com.cursos.alternativos.utils.Commons;

public class MateriasDAO {
	
	private DataSource ds;
	private QueryRunner qr;
	public static MateriasDAO dao;
	
	public MateriasDAO() throws NamingException{
		this.ds = Commons.getDs();
		this.qr = new QueryRunner(ds);
	}
	
	public static MateriasDAO getInstance() throws NamingException{
		if(dao == null) {
			dao = new MateriasDAO();
		}
		return dao;
	}
	
	public List<MateriasDTO> consultaMaterias() throws SQLException{
		String sqlQuery = " SELECT * FROM t_materias WHERE activo=1";
		ResultSetHandler<List<MateriasDTO>> rsh = new BeanListHandler<MateriasDTO>(MateriasDTO.class);
		return qr.query(sqlQuery, rsh);
	}

}
