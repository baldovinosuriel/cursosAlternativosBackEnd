package com.cursos.alternativos.dao;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.BeanMapHandler;

import com.cursos.alternativos.dto.CalificacionesAlumnoDTO;
import com.cursos.alternativos.dto.CalificacionesDTO;
import com.cursos.alternativos.response.CalificacionesAlumnoResponse;
import com.cursos.alternativos.utils.Commons;

public class CalificacionesDAO {
	
	private DataSource ds;
	private QueryRunner qr;
	public static CalificacionesDAO dao;
	
	public CalificacionesDAO() throws NamingException{
		this.ds = Commons.getDs();
		this.qr = new QueryRunner(ds);
	}
	
	public static CalificacionesDAO getInstance() throws NamingException{
		if(dao == null) {
			dao = new CalificacionesDAO();
		}
		return dao;
	}
	
	public List<CalificacionesAlumnoDTO> consultaCalif(int idAlumno) throws SQLException{
		String sqlQuery = " SELECT ta.id_t_usuarios, ta.nombre, ta.ap_paterno, ta.ap_materno,tm.nombre AS materia, "
						+ " tc.id_t_calificaciones, tc.calificacion, to_char(tc.fecha_registro,'dd/mm/yyyy') AS fecha "
						+ " FROM t_alumnos ta, t_materias tm, t_calificaciones tc "
						+ " WHERE tc.id_t_usuarios = ? "
						+ " AND tc.id_t_materias = tm.id_t_materias "
						+ " AND tc.id_t_usuarios = ta.id_t_usuarios ";
		ResultSetHandler<List<CalificacionesAlumnoDTO>> rsh = new BeanListHandler<CalificacionesAlumnoDTO>(CalificacionesAlumnoDTO.class);
		Object[] params = {idAlumno};
		return qr.query(sqlQuery, params, rsh);
	}
	
	public CalificacionesAlumnoResponse promedio(int idAlumno) throws SQLException{
		String sqlQuery = " SELECT avg(calificacion) as promedio"
						+ " FROM t_calificaciones"
						+ " WHERE id_t_usuarios = ?";
		ResultSetHandler<CalificacionesAlumnoResponse> rsh = new BeanHandler<CalificacionesAlumnoResponse>(CalificacionesAlumnoResponse.class);
		Object[] params = {idAlumno};
		return qr.query(sqlQuery, params, rsh );
	}
	
	public int altaCalificacion(CalificacionesDTO calificacionDTO) throws SQLException, ParseException{
		int result = 0;
		String sqlQuery = "INSERT INTO t_calificaciones VALUES (default,?,?,?,?)";
		SimpleDateFormat DateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    String fecha = DateFormat.format(new Date());
	    Date fecha_registro = new SimpleDateFormat("dd/MM/yyyy").parse(fecha); 
	    java.sql.Date sqlDate = new java.sql.Date(fecha_registro.getTime());
		Object[] params = {calificacionDTO.getId_t_materias(), calificacionDTO.getId_t_usuarios(), calificacionDTO.getCalificacion(),sqlDate};
		result = qr.update(sqlQuery,params);
		return result;
	}
	
	public int actualizaCalificacion(BigDecimal calificacion,int idCalificacion) throws SQLException{
		int result = 0;
		String sqlQuery = "UPDATE t_calificaciones SET calificacion = ? WHERE id_t_calificaciones = ?";
		Object[] params = {calificacion, idCalificacion};
		result = qr.update(sqlQuery,params);
		return result;
	}
	
	public int eliminaCalificacion(int idCalificacion) throws SQLException{
		int result = 0;
		String sqlQuery = "DELETE FROM t_calificaciones WHERE id_t_calificaciones = ?";
		Object[] params = {idCalificacion};
		result = qr.update(sqlQuery,params);
		return result;
	}
	
}
