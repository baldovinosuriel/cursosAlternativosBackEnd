package com.cursos.alternativos.dao;

import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.cursos.alternativos.dto.AlumnosDTO;
import com.cursos.alternativos.utils.Commons;

public class AlumnosDAO {
	
	private DataSource ds;
	private QueryRunner qr;
	public static AlumnosDAO dao;
	
	public AlumnosDAO() throws NamingException{
		this.ds = Commons.getDs();
		this.qr = new QueryRunner(ds);
	}
	
	public static AlumnosDAO getInstance() throws NamingException{
		if(dao == null) {
			dao = new AlumnosDAO();
		}
		return dao;
	}
	
	public List<AlumnosDTO> consulta() throws SQLException{
		String sqlQuery = "SELECT * FROM t_alumnos WHERE activo=1";
		ResultSetHandler<List<AlumnosDTO>> rsh = new BeanListHandler<AlumnosDTO>(AlumnosDTO.class);
		return qr.query(sqlQuery, rsh);
	}
	
}
