package com.cursos.alternativos.dto;

public class MateriasDTO {
	
	private int id_t_materias;
	private String nombre;
	private int activo;
	
	public int getId_t_materias() {
		return id_t_materias;
	}
	public void setId_t_materias(int id_t_materias) {
		this.id_t_materias = id_t_materias;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getActivo() {
		return activo;
	}
	public void setActivo(int activo) {
		this.activo = activo;
	}
	
	@Override
	public String toString() {
		return "MateriasDTO [id_t_materias=" + id_t_materias + ", nombre=" + nombre + ", activo=" + activo + "]";
	}

}
