package com.cursos.alternativos.dto;

import java.math.BigDecimal;

public class CalificacionesAlumnoDTO {
	
	private int id_t_usuarios;
	private String nombre;
	private String ap_paterno;
	private String ap_materno;
	private String materia;
	private int id_t_calificaciones;
	private BigDecimal calificacion;
	private String fecha;
	
	public int getId_t_usuarios() {
		return id_t_usuarios;
	}
	public void setId_t_usuarios(int id_t_usuarios) {
		this.id_t_usuarios = id_t_usuarios;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getAp_paterno() {
		return ap_paterno;
	}
	public void setAp_paterno(String ap_paterno) {
		this.ap_paterno = ap_paterno;
	}
	public String getAp_materno() {
		return ap_materno;
	}
	public void setAp_materno(String ap_materno) {
		this.ap_materno = ap_materno;
	}
	public String getMateria() {
		return materia;
	}
	public void setMateria(String materia) {
		this.materia = materia;
	}
	public int getId_t_calificaciones() {
		return id_t_calificaciones;
	}
	public void setId_t_calificaciones(int id_t_calificaciones) {
		this.id_t_calificaciones = id_t_calificaciones;
	}
	public BigDecimal getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(BigDecimal calificacion) {
		this.calificacion = calificacion;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	@Override
	public String toString() {
		return "CalificacionesAlumno [id_t_usuarios=" + id_t_usuarios + ", nombre=" + nombre + ", ap_paterno="
				+ ap_paterno + ", ap_materno=" + ap_materno + ", materia=" + materia + ", id_t_calificaciones="
				+ id_t_calificaciones + ", calificacion=" + calificacion + ", fecha=" + fecha + "]";
	}
	
}
