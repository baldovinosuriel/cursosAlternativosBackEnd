package com.cursos.alternativos.response;

public class Response {
	
	private String succes;
	private String msg;
	
	public String getSucces() {
		return succes;
	}
	public void setSucces(String succes) {
		this.succes = succes;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	@Override
	public String toString() {
		return "Response [succes=" + succes + ", msg=" + msg + "]";
	}

}
