package com.cursos.alternativos.response;

import java.math.BigDecimal;
import java.util.List;

import com.cursos.alternativos.dto.CalificacionesAlumnoDTO;

public class CalificacionesAlumnoResponse {
	
	private List<CalificacionesAlumnoDTO> listaCalificaciones;
	private BigDecimal promedio;
	
	public List<CalificacionesAlumnoDTO> getListaCalificaciones() {
		return listaCalificaciones;
	}
	public void setListaCalificaciones(List<CalificacionesAlumnoDTO> listaCalificaciones) {
		this.listaCalificaciones = listaCalificaciones;
	}
	public BigDecimal getPromedio() {
		return promedio;
	}
	public void setPromedio(BigDecimal promedio) {
		this.promedio = promedio;
	}
	
	@Override
	public String toString() {
		return "CalificacionesAlumnoResponse [listaCalificaciones=" + listaCalificaciones + ", promedio=" + promedio
				+ "]";
	}
	
}
