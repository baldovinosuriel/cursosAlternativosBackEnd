package com.cursos.alternativos.ws;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cursos.alternativos.dao.AlumnosDAO;
import com.cursos.alternativos.dao.CalificacionesDAO;
import com.cursos.alternativos.dao.MateriasDAO;
import com.cursos.alternativos.dto.AlumnosDTO;
import com.cursos.alternativos.dto.CalificacionesAlumnoDTO;
import com.cursos.alternativos.dto.CalificacionesDTO;
import com.cursos.alternativos.dto.MateriasDTO;
import com.cursos.alternativos.response.CalificacionesAlumnoResponse;
import com.cursos.alternativos.response.Response;
import com.google.gson.Gson;

@Controller
public class RestControllerWS {

	@RequestMapping(value = "consultaAlumnos", method = RequestMethod.GET)
	@ResponseBody
	public String consultaAlumnos () {
		List<AlumnosDTO> listaAlumnos = new ArrayList<AlumnosDTO>();
		String json = "";
		try {
		
			listaAlumnos = AlumnosDAO.getInstance().consulta();
			
			json = new Gson().toJson(listaAlumnos);
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	@RequestMapping(value = "consultaCalif", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CalificacionesAlumnoResponse consultaCalif (@RequestParam String idAlumno) {
		CalificacionesAlumnoResponse response = new CalificacionesAlumnoResponse();
		List<CalificacionesAlumnoDTO> listaCalificaciones = new ArrayList<CalificacionesAlumnoDTO>();
		try {
		
			listaCalificaciones = CalificacionesDAO.getInstance().consultaCalif(Integer.valueOf(idAlumno));
			response = CalificacionesDAO.getInstance().promedio(Integer.valueOf(idAlumno));
			
			response.setListaCalificaciones(listaCalificaciones);
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "consultaMaterias", method = RequestMethod.GET)
	@ResponseBody
	public String consultaMaterias () {
		List<MateriasDTO> listaMaterias = new ArrayList<MateriasDTO>();
		String json = "";
		try {
		
			listaMaterias = MateriasDAO.getInstance().consultaMaterias();
			
			json = new Gson().toJson(listaMaterias);
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	@ResponseBody
	@PostMapping(value = "altaCalificacion", produces = MediaType.APPLICATION_JSON_VALUE )
	public Response altaCalificacion (@RequestBody CalificacionesDTO calificacionDTO) {
		Response response = new Response();
		int result = 0;
		try {
		
			result = CalificacionesDAO.getInstance().altaCalificacion(calificacionDTO);
			
			if(result > 0) {
				response.setSucces("ok");
				response.setMsg("calificacion registrada");
			}else {
				response.setSucces("error");
				response.setMsg("calificacion no registrada");
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	@ResponseBody
	@PutMapping(value = "actualizaCalificacion/{id}/{calificacion}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Response actualizaCalificacion (@PathVariable(value = "id") int idCalificacion, 
			@PathVariable(value = "calificacion") BigDecimal calificacion) {
		Response response = new Response();
		int result = 0;
		try {
		
			result = CalificacionesDAO.getInstance().actualizaCalificacion(calificacion, idCalificacion);
			
			if(result > 0) {
				response.setSucces("ok");
				response.setMsg("calificacion actualizada");
			}else {
				response.setSucces("error");
				response.setMsg("calificacion no actualizada");
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	@ResponseBody
	@DeleteMapping(value = "eliminaCalificacion/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Response eliminaCalificacion (@PathVariable(value = "id") int idCalificacion) {
		Response response = new Response();
		int result = 0;
		try {
		
			result = CalificacionesDAO.getInstance().eliminaCalificacion(idCalificacion);
			
			if(result > 0) {
				response.setSucces("ok");
				response.setMsg("calificacion eliminada");
			}else {
				response.setSucces("error");
				response.setMsg("calificacion no eliminada");
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
}
